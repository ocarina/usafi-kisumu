
package {
	import flash.display.Stage;
	import starling.textures.Texture;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	import menu.StartMenu;
	
	/**
	 * ...
	 * @author Emma
	 */
	public class Root extends Sprite
	{
		
		private static var assetManager:AssetManager;
		
		public function Root() 
		{
			
		}
		
		public function start( background:Texture, assets:AssetManager ):void {
			assetManager = assets;
			addChild(new Image(background));
			trace(background);
			assetManager.loadQueue(function onProgress(ratio:Number):void {
				if (ratio == 1) {
					var start:StartMenu = new StartMenu();
					addChild(start);
				}
			});
		}
		
		public static function get assets():AssetManager {
			return assetManager;
		}
		
	}

}