package  {
	import data.Settings;
	import game.characters.Protagonist;
	import game.elements.ScoreKeeper;
    import starling.errors.AbstractClassError;
	import starling.utils.AssetManager;

    public class Constants
    {
        public function Constants() { throw new AbstractClassError(); }
        
        // We chose this stage size because it is used by many mobile devices; 
        // it's e.g. the resolution of the iPhone (non-retina), which means that your game
        // will be displayed without any black bars on all iPhone models up to 4S.
        // 
        // To use landscape mode, exchange the values of width and height, and 
        // set the "aspectRatio" element in the config XML to "landscape". (You'll also have to
        // update the background, startup- and "Default" graphics accordingly.)
        
        public static const STAGE_WIDTH:int  = 480;
        public static const STAGE_HEIGHT:int = 320;
		
		// character states
		public static const STATE_JUMPING:int = 1;
		public static const STATE_DOUBLE_JUMP:int = 2;
		public static const STATE_WALKING:int = 3;
		public static const STATE_STOPPED:int = 4;
		
		// game states
		public static const STATE_MENU:String = "menu";
		public static const STATE_GAME:String = "game";
		public static const STATE_GAMEOVER:String = "dieded";
		
		public static const PLAYER_MOVEMENT_RATE:Number = 2;
		public static const WEAPON_WATER:int = 1;
		public static const WEAPON_SOAP:int = 2;
		
		public static const BUTTON_TYPE_VISIBLE:int = 0;
		public static const BUTTON_TYPE_FIELD:int = 1;
		
		public static var citrus:Main = null;
		public static var buttonType:int = BUTTON_TYPE_VISIBLE;
		public static var scoreKeeper:ScoreKeeper = null;
		public static var settings:Settings = new Settings();
		public static var assets:AssetManager;
    }
}