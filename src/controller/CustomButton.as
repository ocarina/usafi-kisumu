package controller 
{
	import starling.display.Button;
	import starling.textures.Texture;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class CustomButton extends Button
	{
		private var flashTex:Texture;
		private var tex:Texture;
		
		public function CustomButton(texture:Texture, flashTexture:Texture = null, xpos:int = 0, ypos:int = 0) 
		{
			super(texture);
			tex = texture;
			if (flashTex == null) {
				flashTex = tex;
			} else {
				flashTex = flashTexture;
			}
			this.x = xpos;
			this.y = ypos;
		}
		
		public function flash():void {
			setTimeout(stopFlashing, 2000);
			this.upState = flashTex;
		}
		
		private function stopFlashing():void {
			this.upState = tex;
		}
		
	}

}