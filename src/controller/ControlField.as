package controller 
{
	import citrus.objects.platformer.box2d.Hero;
	import flash.display3D.textures.Texture;
	import menu.PauseMenu;
	import starling.display.Button;
	import flash.geom.Rectangle;
	import game.characters.Protagonist;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.Event;
	import starling.events.TouchPhase;
	/**
	 * ...
	 * @author Emma
	 */
	public class ControlField extends Sprite
	{
		public var left:CustomButton;
		public var right:CustomButton;
		public var jump1:CustomButton;
		public var shoot:CustomButton;
		
		private var buttonScale:Number = 1.3;
		
		private var pauseButton:Button;
		private var switchWeapon:Button;
		
		private var char:Protagonist;
		private var jumpBoolean:Boolean = false;
		private var speed:int = 0;
		
		public function ControlField(controllable:Protagonist) 
		{
			char = controllable;
			createButtons();
			createVisibleButtons();
			addEventListeners();
		}
		
		private function createButtons():void {
			left = new CustomButton(Constants.assets.getTexture("control_left.png"));
			left.scaleX = buttonScale;
			left.scaleY = buttonScale;
			left.x = 20;
			left.y = 300 - left.height;
			
			right = new CustomButton(Constants.assets.getTexture("control_right.png"));
			right.scaleX = buttonScale;
			right.scaleY = buttonScale;
			right.x = 20 + left.width + 5;
			right.y = 300 - right.height;
			
			jump1 = new CustomButton(Constants.assets.getTexture("control_jump.png"));
			jump1.scaleX = buttonScale;
			jump1.scaleY = buttonScale;
			jump1.x = 460 - jump1.width;
			jump1.y = 300 - jump1.height;
			
			shoot = new CustomButton(Constants.assets.getTexture("control_shoot.png"));
			shoot.scaleX = buttonScale;
			shoot.scaleY = buttonScale;
			shoot.x = 460 - shoot.width;
			shoot.y = 300 - jump1.height - 5 - shoot.height;
			
			addChild(left);
			addChild(right);
			addChild(jump1);
			addChild(shoot);
		}
		
		private function createVisibleButtons():void {
			pauseButton = new Button(Constants.assets.getTexture("ingame_pauseButton.png"), "");
			pauseButton.x = 5;
			pauseButton.y = 5;
			addChild(pauseButton);
			
			switchWeapon = new Button(Constants.assets.getTexture("ingame_pauseButton.png"), "");
			switchWeapon.x = 240 - switchWeapon.width / 2;
			switchWeapon.y = 310 - switchWeapon.height;
			//addChild(switchWeapon);
		}
		
		private function addEventListeners():void {
			this.addEventListener(TouchEvent.TOUCH, move);
			jump1.addEventListener(TouchEvent.TOUCH, jump);
			shoot.addEventListener(TouchEvent.TOUCH, shootIt);
			
			pauseButton.addEventListener(Event.TRIGGERED, pause);
			switchWeapon.addEventListener(Event.TRIGGERED, switchWeapons);
			
			this.addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function pause():void {
			var menu:PauseMenu = new PauseMenu();
			Constants.citrus.playing = false;
			addChild(menu);
		}
		
		private function switchWeapons():void {
			char.switchWeapon();
		}
		
		private function move(event:TouchEvent):void {
			var touch:Touch = event.getTouch(this);
			var xpos:int = 400;
			if (touch != null) {
				xpos = touch.globalX;
			}
			if (xpos < Constants.STAGE_WIDTH / 2) {
				var split:int = (left.x + left.width + right.x) / 2;
				if (xpos < split) {
					if (event.getTouch(this, TouchPhase.BEGAN) || event.getTouch(this, TouchPhase.MOVED)) {
						speed = -3;
					}
					if (event.getTouch(this, TouchPhase.ENDED)) {
						speed = 0;
					}
				} else {
					if (event.getTouch(this, TouchPhase.BEGAN) || event.getTouch(this, TouchPhase.MOVED)) {
						speed = 3;
					}
					if (event.getTouch(this, TouchPhase.ENDED)) {
						speed = 0;
					}
				}
			}
		}
		
		private function jump(event:TouchEvent):void {
			if (event.getTouch(jump1, TouchPhase.BEGAN)) {
				jumpBoolean = true;
			}
			if (event.getTouch(jump1, TouchPhase.ENDED)) {
				jumpBoolean = false;
			}
		}
		
		private function shootIt(event:TouchEvent):void {
			if (event.getTouch(shoot, TouchPhase.BEGAN)) {
				if (char.getWeaponType() == Constants.WEAPON_SOAP) {
					char.shootSoap();
				} else if (char.getWeaponType() == Constants.WEAPON_WATER) {
					char.throwWater();
				}
			}
		}
		
		public function update():void {
			if (char.controlsEnabled) {
				char.move(jumpBoolean, speed);
			}
		}
		
	}

}