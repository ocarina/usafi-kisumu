package menu 
{
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	/**
	 * ...
	 * @author Emma
	 */
	public class PauseMenu extends Sprite
	{
		private var resumeButton:Button;
		private var helpButton:Button;
		private var settingsButton:Button;
		private var quitButton:Button;
		
		public function PauseMenu() 
		{
			createButtons();
			addActionListeners();
		}
		
		private function createButtons():void {
			resumeButton = new Button(Constants.assets.getTexture("pausemenu_resumeLvlButton.png"), "");
			helpButton = new Button(Constants.assets.getTexture("pausemenu_helpButton.png"), "");
			settingsButton = new Button(Constants.assets.getTexture("mainmenu_optionsButton.png"), "");
			quitButton = new Button(Constants.assets.getTexture("pausemenu_mainMenuButton.png"), "");
			resumeButton.x = Constants.STAGE_WIDTH / 2 - resumeButton.width / 2;
			resumeButton.y = Constants.STAGE_HEIGHT / 5 - resumeButton.height / 2;
			helpButton.x = Constants.STAGE_WIDTH / 2 - helpButton.width / 2;
			helpButton.y = Constants.STAGE_HEIGHT / 5 * 2 - helpButton.height / 2;
			settingsButton.x = Constants.STAGE_WIDTH / 2 - settingsButton.width / 2;
			settingsButton.y = Constants.STAGE_HEIGHT / 5 * 3 - settingsButton.height / 2;
			quitButton.x = Constants.STAGE_WIDTH / 2 - quitButton.width / 2;
			quitButton.y = Constants.STAGE_HEIGHT / 5 * 4 - quitButton.height / 2;
			addChild(resumeButton);
			addChild(helpButton);
			addChild(settingsButton);
			addChild(quitButton);
		}
		
		private function addActionListeners():void {
			resumeButton.addEventListener(Event.TRIGGERED, resume);
			helpButton.addEventListener(Event.TRIGGERED, help);
			settingsButton.addEventListener(Event.TRIGGERED, settings);
			quitButton.addEventListener(Event.TRIGGERED, mainMenu);
		}
		
		private function resume():void {
			Constants.citrus.playing = true;
			this.removeFromParent();
			this.dispose();
		}
		
		private function help():void {
			var newMenu:HelpScreen = new HelpScreen();
			this.parent.addChild(newMenu);
			this.removeFromParent();
		}
		
		private function settings():void {
			var newMenu:SettingsMenu = new SettingsMenu(this);
			this.parent.addChild(newMenu);
			this.removeFromParent();
		}
		
		private function mainMenu():void {
			Constants.citrus.state = new StartMenu();
		}
		
	}

}