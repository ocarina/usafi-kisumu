package menu 
{
	import citrus.view.spriteview.SpriteArt;
	import game.Game;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	/**
	 * ...
	 * @author Emma
	 */
	public class SettingsMenu extends Sprite
	{
		private var backToMain:Button;
		private var switchControls:Button;
		private var previous:Sprite;
		
		public function SettingsMenu(previousScreen:Sprite) 
		{
			previous = previousScreen;
			createButtons();
			addActionListeners();
		}
		
		private function createButtons():void {
			backToMain = new Button(Constants.assets.getTexture("backButton.png"), "");
			backToMain.x = 5;
			backToMain.y = 5;
			addChild(backToMain);
			
			switchControls = new Button(Constants.assets.getTexture("controls.png"), "");
			switchControls.x = Constants.STAGE_WIDTH / 2 - switchControls.width / 2;
			switchControls.y = Constants.STAGE_HEIGHT / 2 - switchControls.height / 2;
			addChild(switchControls);
		}
		
		private function addActionListeners():void {
			backToMain.addEventListener(Event.TRIGGERED, goBack);
			switchControls.addEventListener(Event.TRIGGERED, changeControls);
		}
		
		private function goBack():void {
			this.parent.addChild(previous);
			this.removeFromParent();
			this.dispose();
		}
		
		private function changeControls():void {
			if (Constants.buttonType == Constants.BUTTON_TYPE_FIELD) {
				Constants.buttonType = Constants.BUTTON_TYPE_VISIBLE;
			} else if (Constants.buttonType == Constants.BUTTON_TYPE_VISIBLE) {
				Constants.buttonType = Constants.BUTTON_TYPE_FIELD;
			}
			//var state:Game = Constants.citrus.state as Game;
			//state.setControls();
		}
		
	}

}