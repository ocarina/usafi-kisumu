package menu 
{
	import data.HighScoreManager;
	import feathers.controls.List;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.data.ListCollection;
	import starling.display.Button;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author Emma
	 */
	public class HighScoresScreen extends Sprite
	{
		private var manager:HighScoreManager;
		private var scores:Array;
		private var previous:Sprite;
		
		public function HighScoresScreen(previousScreen:Sprite = null, currentPlayerAndScore:Array = null) 
		{
			manager = HighScoreManager.getManager();
			scores = manager.getScoresAsStrings();
			createButtons();
			createList(); 
		}
		
		private function showList():void {
			var list:List = new List();
			list.width = 250;
			list.height = 200;
			list.x = 0;// Constants.STAGE_WIDTH / 2 - list.width / 2;
			list.y = 80;

			var collection:ListCollection = new ListCollection(scores);
			list.dataProvider = collection;
			//list.itemRendererFactory = function():IListItemRenderer
											//{
												//trace("fgh");
												//var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
												//renderer.label = "test";
												//renderer.labelFunction = function( item:Object ):String {
																			//trace("render items");
																			//return item.name + ": " + item.score;
																		//};
												//return renderer;
											//}
			
			addChild(list);
		}
		
		private function createList():void {
			var text:TextField = new TextField(200, 200, "", "Verdana", 15);
			text.hAlign = "left";
			for (var i:int = 0; i < scores.length; i++) {
				text.text += scores[i] + "\n";
			}
			text.x = 100;
			text.y = 40;
			addChild(text);
		}
		
		private function createButtons():void {
			var backToMain:Button = new Button(Constants.assets.getTexture("backButton.png"), "");
			backToMain.x = 5;
			backToMain.y = 5;
			addChild(backToMain);
			
			backToMain.addEventListener(Event.TRIGGERED, goBack);
		}
		
		private function goBack():void {
			if (previous != null) {
				this.parent.addChild(previous);
			} else {
				this.parent.addChild(new StartMenu());
			}
			this.removeFromParent();
			this.dispose();
		}
		
	}

}