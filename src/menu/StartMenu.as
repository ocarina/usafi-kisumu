package menu 
{
	import citrus.core.starling.StarlingState;
	import game.ChooseCharacter;
	import game.Game;
	import game.LevelOne;
	import game.LevelTwo;
	import game.pointandclick.PointAndClick;
	import starling.display.Image;
	import starling.events.Event;
	import starling.display.Button;
	import starling.display.Sprite;
	/**
	 * ...
	 * @author Emma
	 */
	public class StartMenu extends StarlingState
	{
		private var startGame:Button;
		private var settings:Button;
		private var highScores:Button;
		
		public function StartMenu() 
		{
			addChild(new Image(Constants.assets.getTexture("Main_menu_screen instance 10000")));
			createButtons();
			addActionListeners();
		}
		
		private function createButtons():void {
			startGame = new Button(Constants.assets.getTexture("mainmenu_playbutton.png"), "");
			settings = new Button(Constants.assets.getTexture("mainmenu_optionsButton.png"), "");
			highScores = new Button(Constants.assets.getTexture("mainmenu_LeaderboardButton.png"), "");
			startGame.x = Constants.STAGE_WIDTH / 2 - startGame.width / 2;
			startGame.y = Constants.STAGE_HEIGHT / 4 - startGame.height / 2;
			settings.x = Constants.STAGE_WIDTH / 2 - settings.width / 2;
			settings.y = Constants.STAGE_HEIGHT / 2 - settings.height / 2;
			highScores.x = Constants.STAGE_WIDTH / 2 - highScores.width / 2;
			highScores.y = Constants.STAGE_HEIGHT / 4 * 3 - highScores.height / 2;
			addChild(startGame);
			addChild(settings);
			addChild(highScores);
		}
		
		private function addActionListeners():void {
			startGame.addEventListener(Event.TRIGGERED, goToStart);
			settings.addEventListener(Event.TRIGGERED, goToSettings);
			highScores.addEventListener(Event.TRIGGERED, goToHighScores);
		}
		
		private function goToStart():void {
			this.removeFromParent();
			Constants.citrus.state = new ChooseCharacter();
		}
		
		private function goToSettings():void {
			var newMenu:SettingsMenu = new SettingsMenu(this);
			this.parent.addChild(newMenu);
			this.removeFromParent();
		}
		
		private function goToHighScores():void {
			var newMenu:HighScoresScreen = new HighScoresScreen(this);
			this.parent.addChild(newMenu);
			this.removeFromParent();
		}
		
	}

}