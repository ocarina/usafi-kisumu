package game 
{
	import citrus.core.starling.StarlingState;
	import citrus.objects.platformer.box2d.Platform;
	import game.characters.MonsterRaymond;
	import game.collectables.Soap;
	import menu.StartMenu;
	import starling.display.Button;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.events.Event;
	import citrus.physics.box2d.Box2D;
	/**
	 * ...
	 * @author Emma
	 */
	public class BossOne extends Game
	{
		
		public function BossOne() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("BOSS1.1"), Constants.assets.getTextureAtlas("bosslvl1tiles"));
			
			createProtagonist(350, 1300);
			add(new MonsterRaymond(600, 1500));
			
			createCollectables();
		}
		
		private function createCollectables():void {
			createSoapBars();
			createWaterBuckets();
			createAlcoholGels();
		}
		
		override public function createSoapBars():void {
			add(new Soap(914, 1378));
			add(new Soap(1318, 1533));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			
		}
		
	}

}