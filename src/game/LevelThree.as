package game 
{
	import citrus.objects.platformer.box2d.MovingPlatform;
	import game.characters.MinionAye;
	import game.characters.MinionJouke;
	import game.characters.MinionRaymond;
	import game.collectables.AlcoholGel;
	import game.collectables.QuestionMark;
	import game.collectables.Soap;
	import game.collectables.Water;
	import game.elements.EndLevelTrigger;
	import game.elements.Ladder;
	import game.elements.Trampoline;
	import game.pointandclick.PointAndClick;
	/**
	 * ...
	 * @author ...
	 */
	public class LevelThree extends Game
	{
		
		public function LevelThree() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("LVL3.5"), Constants.assets.getTextureAtlas("lvl3tiles"));
			createProtagonist(270, 1670, true);
			createElements();
			createEnemies();
			createCollectables();
			add(new EndLevelTrigger(BossThree, 1, 3, { x:7430, y:1260, width:5, height:5 }));
		}
		
		private function createEnemies():void {
			add(new MinionJouke(900, 1700));
			add(new MinionAye(2550, 1700));
			add(new MinionRaymond(4455, 845, 60));
			add(new MinionJouke(5000, 1620));
			add(new MinionAye(5255, 1620, 20));
			add(new MinionRaymond(5660, 1620));
			add(new MinionJouke(6200, 1620));
			add(new MinionJouke(5485, 1270));
			
			add(new QuestionMark(821, 1332));
			add(new QuestionMark(2277, 1317));
		}
		
		private function createElements():void {
			add(new Ladder(4775, 1457, 10, 10 * 31 + 10));
			add(new Ladder(6479, 1457, 10, 10 * 31 + 10));
			add(new Ladder(7675, 800, 10, 9 * 31 + 10));
			
			var cloud:MovingPlatform = new MovingPlatform("cloud", { x:3920, y:820, startX:3920, startY:820, endX:4080, endY:820, 
															width:150, height:60, view:Constants.assets.getTexture("wolkje_62px instance 10000") } );
			cloud.enabled = true;
			cloud.oneWay = true;
			add(cloud);
			
			var cloud2:MovingPlatform = new MovingPlatform("cloud", { x:6710, y:960, startX:6710, startY:960, endX:6590, endY:680, width:90, height:60 } );
			cloud2.enabled = true;
			cloud2.oneWay = true;
			add(cloud2);
			
			add(new Trampoline(665, 1600));
			add(new Trampoline(880, 1600));
			add(new Trampoline(1845, 1600));
			add(new Trampoline(2280, 1600));
			add(new Trampoline(5222, 1193));
			add(new Trampoline(6740, 1160));
			add(new Trampoline(6930, 1160));
		}
		
		private function createCollectables():void {
			createSoapBars();
			createAlcoholGels();
			createWaterBuckets();
		}
		
		override public function createSoapBars():void {
			add(new Soap(310, 1530));
			add(new Soap(3110, 1530));
			add(new Soap(4775, 1400));
		}
		
		override public function createWaterBuckets():void {
			add(new Water(1420, 1405));
			add(new Water(3330, 1435));
			add(new Water(4775, 1500));
		}
		
		override public function createAlcoholGels():void {
			add(new AlcoholGel(2275, 1400));
			add(new AlcoholGel(5415, 1600));
		}
		
	}

}