package game 
{
	import citrus.core.starling.StarlingState;
	import flash.events.TimerEvent;
	import game.elements.ScoreKeeper;
	import starling.events.Event;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	/**
	 * ...
	 * @author ...
	 */
	public class HealthQuestion extends Sprite
	{
		private var previous:Game;
		private var xPlayer:int;
		private var yPlayer:int;
		
		private var question:Object;
		private var answerA:Button;
		private var answerB:Button;
		private var answerC:Button;
		private var answerD:Button;
		private var rightAnswer:String;
		
		private var points:int = 500;
		private var questionField:TextField;
		private var timer:Timer;
		
		public function HealthQuestion() 
		{
			super();
			this.addChild(new Image(Constants.assets.getTexture("Pause_Screen instance 10000")));
			displayQuestion();
			timer = new Timer(50, 0);
			timer.addEventListener(TimerEvent.TIMER, decreasePoints);
			timer.start();
		}
		
		private function decreasePoints(e:TimerEvent):void {
			points -= 2;
		}
		
		private function pickQuestion():Object {
			
			var healthQuestion:XML = Constants.assets.getXml("questions");
			var random:int = Math.floor(Math.random() * healthQuestion.children().length());
			
			return { question:healthQuestion.question[random].@text, 
						answerA:healthQuestion.question[random].answer[0].@text, 
						answerB:healthQuestion.question[random].answer[1].@text, 
						answerC:healthQuestion.question[random].answer[2].@text, 
						answerD:healthQuestion.question[random].answer[3].@text, 
						rightAnswer:healthQuestion.question[random].@correct };
		}
		
		private function displayQuestion():void {
			question = pickQuestion();
			questionField = new TextField(300, 70, question.question, "Verdana", 18);
			questionField.x = 70;
			questionField.y = 50;
			answerA = new Button(Constants.assets.getTexture("antwoordvakje instantie 10000"), question.answerA);
			answerB = new Button(Constants.assets.getTexture("antwoordvakje instantie 10000"), question.answerB);
			answerC = new Button(Constants.assets.getTexture("antwoordvakje instantie 10000"), question.answerC);
			answerD = new Button(Constants.assets.getTexture("antwoordvakje instantie 10000"), question.answerD);
			
			answerA.fontSize = 16;
			answerB.fontSize = 16;
			answerC.fontSize = 16;
			answerD.fontSize = 16;
			
			answerA.x = Constants.STAGE_WIDTH / 2 - answerA.width / 2;
			answerB.x = answerA.x;
			answerC.x = answerA.x;
			answerD.x = answerA.x;
			
			answerA.y = 120;
			answerB.y = answerA.y + answerA.height + 5;
			answerC.y = answerB.y + answerB.height + 5;
			answerD.y = answerC.y + answerC.height + 5;
			
			addChild(questionField);
			addChild(answerA);
			addChild(answerB);
			addChild(answerC);
			addChild(answerD);
			
			answerA.addEventListener(Event.TRIGGERED, checkA);
			answerB.addEventListener(Event.TRIGGERED, checkB);
			answerC.addEventListener(Event.TRIGGERED, checkC);
			answerD.addEventListener(Event.TRIGGERED, checkD);
		}
		
		private function checkA():void {
			if (question.rightAnswer == "A") {
				correct();
			} else {
				wrong();
			}
		}
		
		private function checkB():void {
			if (question.rightAnswer == "B") {
				correct();
			} else {
				wrong();
			}
		}
		
		private function checkC():void {
			if (question.rightAnswer == "C") {
				correct();
			} else {
				wrong();
			}
		}
		
		private function checkD():void {
			if (question.rightAnswer == "D") {
				correct();
			} else {
				wrong();
			}
		}
		
		private function correct():void {
			timer.stop();
			if (points < 100) {
				points = 100;
			}
			Constants.scoreKeeper.addPoints(points);
			var image:Image = new Image(Constants.assets.getTexture("Question_correct instantie 10000"));
			image.x = Constants.STAGE_WIDTH / 2 - image.width / 2;
			image.y = Constants.STAGE_HEIGHT / 2 - image.height / 2;
			addChild(image);
			setTimeout(backToGame, 1500);
		}
		
		private function wrong():void {
			timer.stop();
			var image:Image = new Image(Constants.assets.getTexture("Question_incorrect instantie 10000"));
			image.x = Constants.STAGE_WIDTH / 2 - image.width / 2;
			image.y = Constants.STAGE_HEIGHT / 2 - image.height / 2;
			addChild(image);
			setTimeout(backToGame, 1500);
		}
		
		private function backToGame():void {
			Constants.citrus.playing = true;
			this.removeFromParent();
			this.dispose();
		}
		
	}

}