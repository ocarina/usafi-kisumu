package game.collectables 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.platformer.box2d.Coin;
	import citrus.objects.platformer.box2d.Hero;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import game.elements.PlayerData;
	/**
	 * ...
	 * @author Emma
	 */
	public class Water extends Coin
	{
		
		public function Water(xpos:int, ypos:int) 
		{
			super("water", { x:xpos, y:ypos, width:5, height:5, view:Constants.assets.getTexture("Waterbucket instance 10000") } );
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			if (collider is Hero) {
				super.handleBeginContact(contact);
				PlayerData.playerData.addWater(1);
			}
		}
		
	}

}