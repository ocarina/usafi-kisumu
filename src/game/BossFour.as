package game 
{
	import game.characters.MonsterRaymond;
	/**
	 * ...
	 * @author ...
	 */
	public class BossFour extends Game
	{
		
		public function BossFour() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("BOSS1.1"), Constants.assets.getTextureAtlas("bosslvl1tiles"));
			
			createProtagonist(350, 1300);
			add(new MonsterRaymond(600, 1500));
			
			createCollectables();
		}
		
		private function createCollectables():void {
			createSoapBars();
			createWaterBuckets();
			createAlcoholGels();
		}
		
		override public function createSoapBars():void {
			add(new Soap(914, 1378));
			add(new Soap(1318, 1533));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			
		}
		
	}

}