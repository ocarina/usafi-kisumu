package game.elements 
{
	import game.characters.Protagonist;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	/**
	 * ...
	 * @author Emma
	 */
	public class HealthBar extends Sprite
	{
		private var maxHealth:Quad;
		private var health:Quad;
		private var container:Quad;
		private static var instance:HealthBar = null;
		private var playerData:PlayerData = null;
		
		private var char:Protagonist;
		
		public function HealthBar() 
		{
			instance = this;
			playerData = PlayerData.playerData;
			createQuads();
		}
		
		public static function get healthBar():HealthBar {
			return instance;
		}
		
		private function createQuads():void {
			container = new Quad(playerData.maxHP + 4, 12);
			container.color = 0x000000;
			addChild(container);
			
			maxHealth = new Quad(playerData.maxHP, 8);
			maxHealth.color = 0xFF0000;
			maxHealth.x = 2;
			maxHealth.y = 2;
			addChild(maxHealth);
			
			var currentHP:int = playerData.currentHP;
			if (currentHP > 0) {
				health = new Quad(currentHP, 8);
				health.color = 0x00FF00;
				health.x = 2;
				health.y = 2;
				addChild(health);
			}
			
		}
		
		public function update():void {
			var currentHP:int = playerData.currentHP;
			if (currentHP <= 0) {
				removeChild(health);
			} else {
				health.width = currentHP;
			}
		}
		
	}

}