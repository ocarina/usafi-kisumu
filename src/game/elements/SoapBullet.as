package game.elements 
{
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Joints.b2WeldJoint;
	import Box2D.Dynamics.Joints.b2WeldJointDef;
	import citrus.objects.Box2DPhysicsObject;
	import citrus.objects.platformer.box2d.Enemy;
	import citrus.objects.platformer.box2d.Missile;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import citrus.physics.PhysicsCollisionCategories;
	import game.characters.Protagonist;
	import starling.display.MovieClip;
	import starling.events.Event;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class SoapBullet extends Box2DPhysicsObject
	{
		private var hitDestroyTime:int = 500;
		private var direction:int = 1;
		private var destroyTime:int = 2000;
		
		public function SoapBullet(char:Protagonist, toRight:Boolean) 
		{
			super("soapBullet", { width:10, height:10, y:char.y, view:Constants.assets.getTexture("soap instance 10000") } );
			if (!toRight) {
				x = char.x + char.width;
				direction = 1;
			} else {
				x = char.x - char.width;
				direction = -1;
			}
			this.updateCallEnabled = true;
			this.beginContactCallEnabled = true;
			
			setTimeout(handleDestroy, destroyTime);
		}
		
		override public function addPhysics():void {
			super.addPhysics();
			body.ApplyImpulse(new b2Vec2(1.3 * direction, -0.2), body.GetWorldCenter());
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			
			if (collider is Enemy) {
				view = new MovieClip(Constants.assets.getTextures("hitSoap_minion"), 12);
				this.velocity = [direction, 0];
				setTimeout(handleDestroy, hitDestroyTime);
				var jointDef:b2WeldJointDef = new b2WeldJointDef();    
				jointDef.Initialize(body, collider.body, body.GetWorldCenter());    
             
				var joint:b2WeldJoint = b2WeldJoint(_box2D.world.CreateJoint(jointDef));    
				
				updateCallEnabled = false; 
			}
		}
		
		override protected function defineFixture():void {
			super.defineFixture();
			_fixtureDef.filter.maskBits = PhysicsCollisionCategories.GetAllExcept("Items");
		}
		
		public function handleDestroy():void {
			this.kill = true;
		}
	}

}