package game.elements 
{
	import citrus.core.starling.StarlingState;
	import game.Game;
	import game.pointandclick.PointAndClick;
	import starling.display.Button;
	import starling.events.TouchEvent;
	import starling.display.Image;
	import starling.events.Event;
	import flash.utils.setTimeout;
	import starling.events.TouchPhase;
	/**
	 * ...
	 * @author ...
	 */
	public class CutScene extends StarlingState
	{
		private var nextState:Class;
		private var methodAmount:int;
		private var sceneID:String;
		private var imgID:int = 0;
		private var image:Image;
		private var nextButton:Button = new Button(Constants.assets.getTexture("ingame_pauseButton.png"));
		
		public function CutScene(techAmount:int, nextGameState:Class, scene:int) 
		{
			super();
			nextState = nextGameState;
			methodAmount = techAmount;
			sceneID = scene + "_";
			nextButton.x = Constants.STAGE_WIDTH - 5 - nextButton.width;
			nextButton.y = Constants.STAGE_HEIGHT - 5 - nextButton.height;
			nextButton.addEventListener(Event.TRIGGERED, nextScreen);
			nextScreen();
			this.addEventListener(Event.ADDED_TO_STAGE, setTimer);
		}
		
		private function nextScreen(event:Event = null):void {
			nextButton.removeFromParent();
			var imgRef:String = sceneID;
			imgID++;
			imgRef += imgID + ".png";
			trace(imgRef);
			try {
				if (image == null) {
					image = new Image(Constants.assets.getTexture(imgRef));
					addChild(image);
				} else {
					image.texture = Constants.assets.getTexture(imgRef);
				}
				setTimeout(placeNextButton, 1000);
			} catch (e:Error) {
				trace(e.getStackTrace());
				toNextState();
				return;
			}
		}
		
		private function setTimer():void {
			setTimeout(placeNextButton, 1000);
		}
		
		private function placeNextButton():void {
			addChild(nextButton);
		}
		
		private function toNextState():void {
			Constants.citrus.state = new PointAndClick((new nextState()) as Game, methodAmount);
		}
		
	}

}