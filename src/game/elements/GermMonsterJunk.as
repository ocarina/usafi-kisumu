package game.elements 
{
	import Box2D.Common.Math.b2Vec2;
	import citrus.objects.Box2DPhysicsObject;
	import game.characters.GermMonster;
	import flash.utils.setTimeout;
	import starling.display.Image;
	/**
	 * ...
	 * @author Emma
	 */
	public class GermMonsterJunk extends Box2DPhysicsObject
	{
		private var hitDestroyTime:int = 500;
		private var direction:int = 1;
		private var destroyTime:int = 2000;
		
		private var objectStrings:Array = new Array("Apple instance 10000", "Bottle instance 10000", "Mud instance 10000", "slime_throw instance 10000");
		
		public function GermMonsterJunk(monster:GermMonster, toRight:Boolean) 
		{
			super("junk", { width:10, height:10 } );
			setRandomView();
			y = monster.y;
			if (!toRight) {
				x = monster.x + monster.width / 2;
				direction = 1;
			} else {
				x = monster.x - monster.width / 2;
				direction = -1;
			}
			this.updateCallEnabled = true;
			this.beginContactCallEnabled = true;
			
			setTimeout(handleDestroy, destroyTime);
		}
		
		public function setRandomView():void {
			var randomInt:int = Math.floor(Math.random() * objectStrings.length);
			this.view = new Image(Constants.assets.getTexture(objectStrings[randomInt] as String));
		}
		
		override public function addPhysics():void {
			super.addPhysics();
			body.ApplyImpulse(new b2Vec2(0.5 * direction, -1), body.GetWorldCenter());
		}
		
		private function handleDestroy():void {
			this.kill = true;
		}
		
	}

}