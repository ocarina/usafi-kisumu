package game.elements 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.platformer.box2d.Enemy;
	import citrus.objects.platformer.box2d.Sensor;
	import starling.display.MovieClip;
	import game.characters.GermMonster;
	/**
	 * ...
	 * @author ...
	 */
	public class DirtyWater extends Sensor
	{
		
		public function DirtyWater(xpos:int, ypos:int) 
		{
			super("dirtyWater", { x:xpos, y:ypos, width:25, height:25, view:new MovieClip(Constants.assets.getTextures("water_rock instance"), 12) } );
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			onBeginContact.dispatch(contact);
			trace('ok');
		}
		
	}

}