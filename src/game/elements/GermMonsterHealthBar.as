package game.elements 
{
	import citrus.objects.CitrusSprite;
	import game.characters.GermMonster;
	import starling.display.Quad;
	import starling.display.Sprite;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author ...
	 */
	public class GermMonsterHealthBar extends CitrusSprite
	{
		private var background:Quad = null;
		private var redBar:Quad = null;
		private var greenBar:Quad = null;
		
		private var maxHealth:int = 1;
		private var germMonster:GermMonster;
		
		public function GermMonsterHealthBar(monster:GermMonster) 
		{
			super("monsterHealthBar");
			maxHealth = monster.currentHP;
			germMonster = monster;
			this.x = monster.x;
			this.y = monster.y - monster.height / 2 - 30;
			this.updateCallEnabled = true;
		}
		
		override public function initialize(poolObjectParams:Object = null):void {
			super.initialize(poolObjectParams);
			createQuads();
			Constants.citrus.state.add(this);
		}
		
		private function createQuads():void {
			background = new Quad(32, 4, 0x000000);
			redBar = new Quad(30, 2, 0xFF0000);
			redBar.x = 1;
			redBar.y = 1;
			greenBar = new Quad(30, 2, 0x00FF00);
			greenBar.x = 1;
			greenBar.y = 1;
			
			var sprite:Sprite = new Sprite();
			sprite.addChild(background);
			sprite.addChild(redBar);
			sprite.addChild(greenBar);
			this.view = sprite;
		}
		
		public function updateHealth():void {
			if (germMonster != null) {
				if (germMonster.currentHP <= 0) {
					greenBar.removeFromParent();
				} else {
					greenBar.width = 30 / maxHealth * germMonster.currentHP;
				}
			} 
		}
		
		override public function update(timeDelta:Number):void {
			if (germMonster.currentHP > 0) {
				super.update(timeDelta);
				this.x = germMonster.x - this.width / 2;
				this.y = germMonster.y - germMonster.height / 2 - 30;
			} else {
				updateCallEnabled = false;
				setTimeout(killIt, 1000);
			}
		}
		
		private function killIt():void {
			this.kill = true;
		}
		
	}

}