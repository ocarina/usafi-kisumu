package game.elements 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.platformer.box2d.Coin;
	import game.Game;
	import game.LevelOne;
	/**
	 * ...
	 * @author Emma
	 */
	public class TutorialTrigger extends Coin
	{
		private var counter:int;
		
		public function TutorialTrigger(xpos:int, ypos:int,screenToShow:int = 0) 
		{
			super("tutorial", { x:xpos, y:ypos, width:10, height:10 } );
			counter = screenToShow;
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			super.handleBeginContact(contact);
			var state:Game = null;
			try {
				state = Constants.citrus.state as Game;
			} catch (error:Error) {
				trace("Current screen is not a level");
				return;
			}
			state.handleTutorial(counter, true);
			
		}
		
	}

}