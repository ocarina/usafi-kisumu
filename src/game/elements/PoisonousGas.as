package game.elements 
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.Box2DPhysicsObject;
	import citrus.objects.platformer.box2d.Coin;
	import citrus.objects.platformer.box2d.Sensor;
	import citrus.physics.PhysicsCollisionCategories;
	import game.characters.GermMonster;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class PoisonousGas extends Sensor
	{
		private var germMonster:GermMonster;
		
		public function PoisonousGas(monster:GermMonster) 
		{
			super("gas", { x:monster.x, y:monster.y - 15, width:monster.width + 10, height:monster.height + 10 } );
			germMonster = monster;
			setTimeout(handleDestroy, 3000);
			
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			super.handleBeginContact(contact);
		}
		
		private function handleDestroy():void {
			this.kill = true;
		}
		
	}

}