package game.elements 
{
	import game.characters.GermMonster;
	/**
	 * ...
	 * @author Emma
	 */
	public class ScoreKeeper 
	{
		private var score:int;
		
		public function ScoreKeeper() 
		{
			score = 0;
			Constants.scoreKeeper = this;
		}
		
		/**
		 * Gets the score bonus belonging to an enemy and adds it to the score.
		 * @param	enemy
		 */
		public function enemyDefeated(enemy:GermMonster):void {
			score += enemy.bonusScore;
		}
		
		/**
		 * Function to add the time bonus at the end of a level. The time parameter is the time spent to 
		 * finish the level in seconds, the bonusTime parameter is the amount of time in seconds the player
		 * has to finish a level to get a time bonus.
		 * @param	time
		 * @param	bonusTime
		 */
		public function addTimeBonus(time:int, bonusTime:int):int {
			if (bonusTime <= time) {
				return 0;
			}
			var bonus:int = (bonusTime - time) * 10;
			score += bonus;
			return bonus;
		}
		
		public function addPoints(points:int):void {
			score += points;
		}
		
		public function addHealthBonus(health:int, maxHealth:int):int {
			score += health;
			return health;
		}
		
		public function get currentScore():int {
			return score;
		}
		
		/**
		 * For resuming a saved game
		 */
		public function setCurrentScore(score:int):void {
			this.score = score;
		}
		
	}

}