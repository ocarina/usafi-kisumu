package game.elements 
{
	import data.Settings;
	import game.Game;
	/**
	 * ...
	 * @author Emma
	 */
	public class PlayerData 
	{
		private static var instance:PlayerData = null;
		
		private var soap:int;
		private var water:int;
		
		private var health:int;
		private var maxHealth:int;
		private var playerName:String;
		private var gender:String;
		
		public function PlayerData(name:String, boyOrGirl:String) 
		{
			instance = this;
			playerName = name;
			gender = boyOrGirl;
			soap = 0;
			water = 0;
			health = 100;
			maxHealth = 100;
		}
		
		public static function get playerData():PlayerData {
			return instance;
		}
		
		public function get boyOrGirl():String {
			return gender;
		}
		
		public function get name():String {
			return playerName;
		}
		
		public function addSoap(amount:int):void {
			soap += amount;
			if (!Constants.settings.getTutorialTriggered() && water > 0) {
				continueTutorial();
			}
		}
		
		public function addWater(amount:int):void {
			water += amount;
			if (!Constants.settings.getTutorialTriggered() && soap > 0) {
				continueTutorial();
			}
		}
		
		private function continueTutorial():void {
			Constants.settings.setTutorialTriggered(true);
			var state:Game = Constants.citrus.state as Game;
			state.handleTutorial(6, true);
		}
		
		public function useSoap():Boolean {
			if (soap > 0) {
				soap--;
				return true;
			}
			return false;
		}
		
		public function useWater():Boolean {
			if (water > 0) {
				water--;
				return true;
			}
			
			return false;
		}
		
		public function getSoapAmount():int {
			return soap;
		}
		
		public function getWaterAmount():int {
			return water;
		}
		
		public function loseHealth(amount:int):Boolean {
			health -= amount;
			HealthBar.healthBar.update();
			return health <= 0;
		}
		
		public function replenishHealth(amount:int):void {
			health += amount;
			if (health > maxHealth)
				health = maxHealth;
			HealthBar.healthBar.update();
		}
		
		public function get maxHP():int {
			return maxHealth;
		}
		
		public function get currentHP():int {
			return health;
		}
		
		public function reset():void {
			soap = 0;
			water = 0;
			maxHealth = 100;
			health = maxHealth;
		}
		
	}

}