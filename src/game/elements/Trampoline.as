package game.elements 
{
	import citrus.objects.platformer.box2d.Platform;
	import starling.display.Quad;
	/**
	 * ...
	 * @author ...
	 */
	public class Trampoline extends Platform
	{
		
		public function Trampoline(xpos:int, ypos:int, tWidth:int = 155, tHeight:int = 20) 
		{
			super("trampoline", { x:xpos, y:ypos, width:tWidth, height:tHeight } );
			oneWay = true;
			if (_box2D.visible == true) {
				var quad:Quad = new Quad(tWidth, tHeight, 0xFE2EF7);
				quad.alpha = 50;
				this.view = quad;
			}
		}
		
	}

}