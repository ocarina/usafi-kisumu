package game.elements 
{
	import citrus.objects.platformer.box2d.Sensor;
	import starling.display.Quad;
	/**
	 * ...
	 * @author ...
	 */
	public class Ladder extends Sensor
	{
		
		public function Ladder(xpos:int, ypos:int, lWidth:int = 10, lHeight:int = 103) 
		{
			super("ladder", { x:xpos, y:ypos, width:lWidth, height:lHeight } );
			if (_box2D.visible == true) {
				var quad:Quad = new Quad(lWidth, lHeight, 0xFFFFFF);
				quad.alpha = 50;
				this.view = quad;
			}
		}
		
	}

}