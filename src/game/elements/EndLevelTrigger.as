package game.elements 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.core.IState;
	import citrus.core.starling.StarlingState;
	import citrus.objects.platformer.box2d.Coin;
	import citrus.objects.platformer.box2d.Hero;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import game.BossOne;
	import game.pointandclick.PointAndClick;
	import starling.display.Image;
	/**
	 * ...
	 * @author Emma
	 */
	public class EndLevelTrigger extends Coin
	{
		private var next:Class;
		private var methodCount:int;
		private var scene:int;
		
		public function EndLevelTrigger(nextLevel:Class, sceneID:int, techCount:int, params:Object) 
		{
			super("endLevel", params);
			methodCount = techCount;
			scene = sceneID;
			this.view = new Image(Constants.assets.getTexture("Bord handwash instance 10000"));
			next = nextLevel;
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			if (collider is Hero) {
				Constants.citrus.state = new CutScene(methodCount, next, scene);
			}
		}
		
	}

}