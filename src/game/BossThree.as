package game 
{
	import game.characters.MonsterAye;
	import game.characters.MonsterJouke;
	import game.collectables.Soap;
	import game.elements.Trampoline;
	/**
	 * ...
	 * @author ...
	 */
	public class BossThree extends Game
	{
		
		public function BossThree() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("BOSS3"), Constants.assets.getTextureAtlas("lvl3tiles"));
			
			createProtagonist(350, 1520);
			add(new MonsterJouke(600, 1600));
			
			createCollectables();
			
			add(new Trampoline(449, 1503));
			add(new Trampoline(1100, 1503));
		}
		
		private function createCollectables():void {
			createSoapBars();
			createWaterBuckets();
			createAlcoholGels();
		}
		
		override public function createSoapBars():void {
			add(new Soap(320, 1550));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			
		}
		
	}

}