package game 
{
	import citrus.core.CitrusObject;
	import citrus.core.starling.StarlingState;
	import citrus.objects.platformer.box2d.Platform;
	import citrus.utils.objectmakers.ObjectMakerStarling;
	import controller.ControlField;
	import data.Settings;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import game.elements.PlayerData;
	import game.elements.ScoreKeeper;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import game.characters.Protagonist;
	import game.elements.HealthBar;
	import menu.PauseMenu;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.TextureAtlas;
	import citrus.physics.box2d.Box2D;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class Game extends StarlingState
	{
		protected var protagonist:Protagonist;
		protected var healthBar:HealthBar;
		protected var scoreField:TextField;
		
		private var soapAmountHolder:Button;
		private var waterAmountHolder:Button;
		
		
		private var tutorialCount:int = 0;
		private var currentImage:Image = null;
		private var charImage:Image = null;
		
		public function Game() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			var box2d:Box2D = new Box2D("box2d");
			box2d.visible = false;
			add(box2d);
		}
		
		public function setControls():void {
			if (protagonist != null) {
				addChild(new ControlField(protagonist));
			}
		}
		
		protected function createLevel(xml:XML, atlas:TextureAtlas):void {
			ObjectMakerStarling.FromTiledMap(xml, atlas);
			
			var platforms:Vector.<CitrusObject> = this.getObjectsByName("jumpThrough");
			platforms.forEach(alterPlatforms);
		}
		
		private function alterPlatforms(platform:CitrusObject, index:int, vector:Vector.<CitrusObject>):void {
			var currentPlatform:Platform = platform as Platform;
			currentPlatform.oneWay = true;
		}
		
		protected function createPlayerInfo():void {
			healthBar = new HealthBar();
			healthBar.x = 475 - healthBar.width;
			healthBar.y = 5;
			addChild(healthBar);
			
			soapAmountHolder = new Button(Constants.assets.getTexture("rondje zeep instance 10000"), PlayerData.playerData.getSoapAmount() + "");
			waterAmountHolder = new Button(Constants.assets.getTexture("rondje water instance 10000"), PlayerData.playerData.getWaterAmount() + "");
			soapAmountHolder.x = Constants.STAGE_WIDTH - 5 - soapAmountHolder.width;
			waterAmountHolder.x = Constants.STAGE_WIDTH - 5 - waterAmountHolder.width;
			soapAmountHolder.y = 52;
			waterAmountHolder.y = 87;
			soapAmountHolder.fontBold = true;
			waterAmountHolder.fontBold = true;
			
			addChild(soapAmountHolder);
			addChild(waterAmountHolder);
			
		}
		
		protected function createProtagonist(xpos:int = 350, ypos:int = 750, reset:Boolean = false):void {
			protagonist = new Protagonist(xpos, ypos);
			if (reset) {
				PlayerData.playerData.reset();
				new ScoreKeeper();
			}
			createPlayerInfo();
			add(protagonist);
			setControls();
			camera.setUp(protagonist);
			createScoreHolder();
		}
		
		public function get player():Protagonist {
			return protagonist;
		}
		
		public function onPause():void {
			var menu:PauseMenu = new PauseMenu();
			Constants.citrus.playing = false;
			addChild(menu);
		}
		
		public function createScoreHolder():void {
			trace("test score");
			scoreField = new TextField(100, 25, Constants.scoreKeeper.currentScore + "", "Verdana", 18, 0xFFFFFF);
			scoreField.x = 60;
			scoreField.y = 5;
			scoreField.hAlign = "left";
			addChild(scoreField);
		}
		
		override public function update(timeDelta:Number):void {
			super.update(timeDelta);
			scoreField.text = Constants.scoreKeeper.currentScore + "";
			soapAmountHolder.text = PlayerData.playerData.getSoapAmount() + "";
			waterAmountHolder.text = PlayerData.playerData.getWaterAmount() + "";
		}
		
		private function _handleTutorial(event:TouchEvent = null):void {
			if (event == null) {
				handleTutorial(tutorialCount + 1, true);
				return;
			}
			if (event.getTouch(this, TouchPhase.BEGAN))
				handleTutorial(tutorialCount + 1);
		}
		
		public function handleTutorial(counter:int = 0, addListener:Boolean = false):void {
			tutorialCount = counter;
			Constants.citrus.playing = false;
			if (addListener) {
				this.addEventListener(TouchEvent.TOUCH, _handleTutorial);
				trace("event listener added");
			}
			switch (tutorialCount) {
				case 0:
					currentImage = new Image(Constants.assets.getTexture("tutorial_screen01 instance 10000"));
					addChild(currentImage);
					break;
				case 1:
					currentImage.removeFromParent();
					currentImage = new Image(Constants.assets.getTexture("tutorial_screen02 instance 10000"));
					addChild(currentImage);
					if (PlayerData.playerData.boyOrGirl == "boy") {
						charImage = new Image(Constants.assets.getTexture("male_tutorial instance 10000"));
					} else {
						charImage = new Image(Constants.assets.getTexture("female_tutorial instance 10000"));
					}
					charImage.x = 280;
					charImage.y = 60;
					addChild(charImage);
					break;
					
				case 3:
					currentImage = new Image(Constants.assets.getTexture("tutorial_screen03 instance 10000"));
					addChild(currentImage);
					break;
				case 4:
					currentImage.texture = Constants.assets.getTexture("tutorial_screen04 instance 10000");
					break;
					
				case 6:
					currentImage.texture = Constants.assets.getTexture("tutorial_screen05 instance 10000");
					addChild(currentImage);
					break;
					
				case 8:
					currentImage.texture = Constants.assets.getTexture("tutorial_screen06 instance 10000");
					addChild(currentImage);
					break;
					
				
				
				default:
					removeEventListener(TouchEvent.TOUCH, _handleTutorial);
					Constants.citrus.playing = true;
					if (tutorialCount == 2) {
						setTimeout(_handleTutorial, 2000);
					}
					if (currentImage.parent != null)
						currentImage.removeFromParent();
					if (charImage.parent != null)
						charImage.removeFromParent();
					trace("event listener removed");
					return;
			}
		}
		
		public function createSoapBars():void {
			// override for placing soap bars in the level
		}
		
		public function createAlcoholGels():void {
			// override for placing alcohol gels
		}
		
		public function createWaterBuckets():void {
			// override for placing water buckets
		}
		
	}

}