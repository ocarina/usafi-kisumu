package game 
{
	import citrus.core.starling.StarlingState;
	import game.characters.MinionAye;
	import game.characters.MinionJouke;
	import game.characters.MinionRaymond;
	import game.collectables.AlcoholGel;
	import game.collectables.QuestionMark;
	import game.collectables.Soap;
	import game.elements.EndLevelTrigger;
	import game.pointandclick.PointAndClick;
	/**
	 * ...
	 * @author Emma
	 */
	public class LevelTwo extends Game
	{
		
		public function LevelTwo() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("LVL2"), Constants.assets.getTextureAtlas("lvl2tiles"));
			createProtagonist(350, 1500);
			createEnemies();
			createCollectables();
			add(new EndLevelTrigger(BossTwo, 1, 3, { x:7060, y:1700, width:5, height:5 }));
		}
		
		private function createEnemies():void {
			add(new MinionRaymond(1130, 1500));
			add(new MinionAye(1400, 1500));
			add(new MinionRaymond(950, 900));
			add(new MinionAye(2440, 900));
			add(new MinionAye(2000, 1500));
			add(new MinionAye(3100, 900));
			add(new MinionRaymond(3110, 1500, 20));
			add(new MinionAye(3600, 1500));
			add(new MinionRaymond(4180, 1600));
			add(new MinionAye(5720, 1600));
		}
		
		private function createCollectables():void {
			createSoapBars();
			createAlcoholGels();
			createWaterBuckets();
			
			add(new QuestionMark(4866, 1580));
		}
		
		override public function createSoapBars():void {
			add(new Soap(350, 1624));
			add(new Soap(1490, 1645));
			add(new Soap(5549, 1645));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			add(new AlcoholGel(6494, 1350));
			add(new AlcoholGel(3830, 1500));
			add(new AlcoholGel(3349, 1293));
		}
		
	}

}