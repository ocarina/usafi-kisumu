package game 
{
	import game.characters.MonsterAye;
	import game.characters.MonsterRaymond;
	import game.collectables.Soap;
	/**
	 * ...
	 * @author Emma
	 */
	public class BossTwo extends Game
	{
		
		public function BossTwo() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("BOSS2.1"), Constants.assets.getTextureAtlas("boss2tiles"));
			
			createProtagonist(350, 1520);
			add(new MonsterAye(600, 1600));
			createCollectables();
		}
		
		private function createCollectables():void {
			createSoapBars();
			createWaterBuckets();
			createAlcoholGels();
		}
		
		override public function createSoapBars():void {
			add(new Soap(600, 1550));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			
		}
		
	}

}