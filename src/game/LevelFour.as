package game 
{
	import game.characters.MinionAye;
	import game.characters.MinionJouke;
	import game.characters.MinionRaymond;
	import game.collectables.AlcoholGel;
	import game.collectables.QuestionMark;
	import game.collectables.Soap;
	import game.collectables.Water;
	/**
	 * ...
	 * @author ...
	 */
	public class LevelFour extends Game
	{
		
		public function LevelFour() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			createLevel(Constants.assets.getXml("LVL4"), Constants.assets.getTextureAtlas("lvl4tiles"));
			createProtagonist(350, 1000, true);
			createEnemies();
			createCollectables();
			add(new EndLevelTrigger(new PointAndClick(new BossFour()), { x:7060, y:1700, width:5, height:5 }));
		}
		
		private function createEnemies():void {
			add(new MinionAye(1435, 1180));
		}
		
		private function createCollectables():void {
			createSoapBars();
			createAlcoholGels();
			createWaterBuckets();
			
			add(new QuestionMark(4866, 1580));
		}
		
		override public function createSoapBars():void {
			add(new Soap(350, 1624));
			add(new Soap(1490, 1645));
			add(new Soap(5549, 1645));
		}
		
		override public function createWaterBuckets():void {
			
		}
		
		override public function createAlcoholGels():void {
			add(new AlcoholGel(6494, 1350));
			add(new AlcoholGel(3830, 1500));
			add(new AlcoholGel(3349, 1293));
		}
		
	}

}