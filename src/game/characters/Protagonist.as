package game.characters 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.box2d.Hero;
	import citrus.objects.platformer.box2d.Sensor;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import citrus.physics.PhysicsCollisionCategories;
	import citrus.view.starlingview.AnimationSequence;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Vec2;
	import dragonBones.animation.Animation;
	import game.elements.DirtyWater;
	import game.elements.GermMonsterJunk;
	import game.elements.Ladder;
	import game.elements.PlayerData;
	import game.elements.PoisonousGas;
	import game.elements.Trampoline;
	import game.Game;
	import starling.display.MovieClip;
	import game.BossOne;
	import game.elements.HealthBar;
	import game.elements.SoapBullet;
	import game.GameOver;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class Protagonist extends Hero
	{
		
		private var health:int;
		private var maxHealth:int = 100;
		private var weaponType:int = Constants.WEAPON_SOAP;
		private var girl:Boolean = true;
		private var shooting:Boolean = false;
		
		private var walk:MovieClip;
		private var jump:MovieClip;
		private var idle:MovieClip;
		private var gotHurt:MovieClip;
		private var shoot:MovieClip;
		private var die:MovieClip;
		private var climb:MovieClip;
		
		private var _onLadder:Boolean = false;
		private var _climbing:Boolean = false;
		private var climbUpVelocity:Number = 2.5;
		private var climbDownVelocity:Number;
		private var originalGravity:b2Vec2;
		private var ladder:Ladder;
		private var canClimb:Boolean;
		
		private var launchSpeed:int = 5;
		private var maxLaunch:int = 20;
		
		public function Protagonist(xpos:int = 350, ypos:int = 1000) 
		{
			super("hero", { x:xpos, y:ypos, width:25, height:80 } );
			health = maxHealth;
			hurtVelocityX = 4;
			hurtVelocityY = 7;
			hurtDuration = 500;
			animate();
			originalGravity = _box2D.gravity;
		}
		
		public function move(jumping:Boolean, speed:int):void {
			var vel:b2Vec2 = body.GetLinearVelocity();
			var desiredVel:Number = 0;
			var direction:Number = 0;
		   
			// determine direction
			if (speed < 0) {
				direction = -1;
			} else if (speed > 0) {
				direction = 1;
			}
		   
			// determine speed
			switch ( direction ) {
				case  0:  
					desiredVel = vel.x * 0.98; 
					break;
				default: 
					desiredVel = b2Math.Min( vel.x + Constants.PLAYER_MOVEMENT_RATE, -Math.abs(speed) ); 
					break;
			}
		   
			if (speed > 0) {
				desiredVel = -desiredVel;
			}
		   
			var velChange:Number = desiredVel - vel.x;
			var xImpulse:Number = body.GetMass() * velChange; 
		   
			// jumping
			var yImpulse:Number = 0;
			if (jumping && this._onGround && !canClimb) {
				yImpulse = body.GetMass() * -jumpHeight;
				this._onGround = false;
			} else if (jumping && canClimb) {
				_onLadder = _climbing = true;
				velocity = new Array( 0, -climbUpVelocity);
			} 
			
			
			/*debugging
			if(xImpulse != 0) {
				trace(xImpulse);
			}*/
			if (speed != 0) {
				this._fixture.SetFriction(0);
			} else {
				this._fixture.SetFriction(_friction);
			}
			body.ApplyImpulse(new b2Vec2(xImpulse,yImpulse), body.GetWorldCenter() );
		}
		
		/**
		 * 
		 * @param	weapon
		 * @return 	new weapon
		 */
		public function switchWeapon():int {
			if (weaponType == Constants.WEAPON_SOAP) {
				weaponType = Constants.WEAPON_WATER;
				trace("water");
			} else if (weaponType == Constants.WEAPON_WATER) {
				weaponType = Constants.WEAPON_SOAP;
				trace("soap");
			}
			return weaponType;
		}
		
		public function getWeaponType():int {
			return weaponType;
		}
		
		public function throwWater():void {
			trace("TODO: throw water");
		}
		
		public function shootSoap():void {
			if (PlayerData.playerData.useSoap()) {
				shooting = true;
				setTimeout(createSoapBullet, 50);
				setTimeout(stopShooting, 500);
			} 
			if (PlayerData.playerData.getSoapAmount() <= 0) {
				var state:Game = Constants.citrus.state as Game;
				if (state.getObjectsByName("soap").length <= 0) {
					state.createSoapBars();
				}
			}
		}
		
		private function createSoapBullet():void {
			Constants.citrus.state.add(new SoapBullet(this, inverted));
		}
		
		public function stopShooting():void {
			shooting = false;
		}
		
		private function animate():void {
			if (PlayerData.playerData.boyOrGirl == "boy") {
				walk = new MovieClip(Constants.assets.getTextures("Male_Walk"), 12);
				jump = new MovieClip(Constants.assets.getTextures("Male_Jump"), 6);
				idle = new MovieClip(Constants.assets.getTextures("Idle_Male"), 12);
				gotHurt = new MovieClip(Constants.assets.getTextures("Male_gotHit"), 12);
				shoot = new MovieClip(Constants.assets.getTextures("Male_Shoot"), 12);
				die = new MovieClip(Constants.assets.getTextures("Male_Death"), 12);
				climb = new MovieClip(Constants.assets.getTextures("Boy_Ladder_Animation"), 12);
			} else {
				walk = new MovieClip(Constants.assets.getTextures("Female_Walk"), 12);
				jump = new MovieClip(Constants.assets.getTextures("Female_Jump"), 6);
				idle = new MovieClip(Constants.assets.getTextures("Idle_Female"), 12);
				gotHurt = new MovieClip(Constants.assets.getTextures("Female_gotHit"), 12);
				shoot = new MovieClip(Constants.assets.getTextures("Female_Shoot"), 12);
				die = new MovieClip(Constants.assets.getTextures("Female_Death"), 12);
				climb = new MovieClip(Constants.assets.getTextures("female_ladder_animation"), 12);
			}
			//var aniSeq:AnimationSequence = new AnimationSequence(Main.assets.getTextureAtlas("Characters_Monsters_Objects"), [idle, walk, jump, gotHurt], idle, 12, true);
			
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			
			if (collider is Trampoline) {
				var tramp:Trampoline = collider as Trampoline;
				trace(this.y + " " + tramp.y);
				if (_body.GetLinearVelocity().y > 0 && this.y + (this.height / 2) - 5 <= tramp.y ) {
					var launch:int = launchSpeed + _body.GetLinearVelocity().y;
					if (launch > maxLaunch) launch = maxLaunch;
					_body.SetLinearVelocity(new b2Vec2(0, -launch));
				}
			}
			
			if (collider is Ladder) {
				canClimb = true;
				ladder = collider as Ladder;
			}
			
			if (_enemyClass && collider is GermMonster || collider is GermMonsterSlime || collider is PoisonousGas || collider is GermMonsterJunk || collider is DirtyWater)
			{
				if (collider is GermMonster) {
					var monster:GermMonster = collider as GermMonster;
					PlayerData.playerData.loseHealth(monster.hurtPoints);
				} else if (collider is DirtyWater) {
					PlayerData.playerData.loseHealth(2);
					if (PlayerData.playerData.currentHP <= 0) {
						hurt();
					}
					return;
				} else {
					PlayerData.playerData.loseHealth(7);
				}
				hurt();
					
				//fling the hero
				var hurtVelocity:b2Vec2 = _body.GetLinearVelocity();
				hurtVelocity.y = -hurtVelocityY;
				hurtVelocity.x = hurtVelocityX;
				if (collider.x > x)
					hurtVelocity.x = -hurtVelocityX;
				_body.SetLinearVelocity(hurtVelocity);
			}
			
			//Collision angle if we don't touch a Sensor.
			if (contact.GetManifold().m_localPoint && !(collider is Sensor)) //The normal property doesn't come through all the time. I think doesn't come through against sensors.
			{	
				var collisionAngle:Number = Math.atan2(contact.normal.y, contact.normal.x);
				
				if (collisionAngle >= Math.PI*.25 && collisionAngle <= 3*Math.PI*.25 ) // normal angle between pi/4 and 3pi/4
				{
					_groundContacts.push(collider.body.GetFixtureList());
					_onGround = true;
					updateCombinedGroundAngle();
				}
			}
			
		}
		
		override public function handleEndContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			
			if (collider is Ladder) {
				canClimb = _climbing = _onLadder = false;
			}
			
			//Remove from ground contacts, if it is one.
			var index:int = _groundContacts.indexOf(collider.body.GetFixtureList());
			if (index != -1) {
				_groundContacts.splice(index, 1);
				if (_groundContacts.length == 0)
					_onGround = false;
				updateCombinedGroundAngle();
			}
		}
		
		override public function hurt():void {
			if (PlayerData.playerData.currentHP <= 0) {
				hurtDuration = 1400;
			}
			super.hurt();
		}
		
		override protected function endHurtState():void {
			super.endHurtState();
			if (PlayerData.playerData.currentHP <= 0) {
				Constants.citrus.state = new GameOver();
			}
		}
		
		override protected function updateAnimation():void {
			
			//var prevAnimation:String = _animation;
			
			var prevView:* = view;
			
			var walkingSpeed:Number = getWalkingSpeed();
			
			if (_hurt) {
				if (PlayerData.playerData.currentHP <= 0) {
					view = die;
				} else {
					view = gotHurt;//_animation = gotHurt;
				}
			}
				
			else if (!_onGround) {
				
				if (_onLadder) {
					view = climb;
				} else {
					view = jump;//_animation = jump;
				}
				
				if (walkingSpeed < -acceleration)
					_inverted = true;
				else if (walkingSpeed > acceleration)
					_inverted = false;
				
			} else if (shooting)
				view = shoot;//_animation = "duck";
				
			else {
				
				if (walkingSpeed < -acceleration) {
					_inverted = true;
					 view = walk;//_animation = walk;
					
				} else if (walkingSpeed > acceleration) {
					
					_inverted = false;
					view = walk;//_animation = walk;
					
				} else
					view = idle;//_animation = idle;
			}
			
			//if (prevAnimation != _animation)
			if (prevView != view)
				onAnimationChange.dispatch();
		}
		
	}

}