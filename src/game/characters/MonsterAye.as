package game.characters 
{
	import game.GameOver;
	import game.LevelThree;
	import game.LevelTwo;
	import menu.StartMenu;
	/**
	 * ...
	 * @author ...
	 */
	public class MonsterAye extends GermMonster
	{
		
		public function MonsterAye(xpos:int, ypos:int) 
		{
			super("RaymondBoss", { x:xpos, y:ypos, width:140, height:120 } );
			setUp();
		}
		
		private function setUp():void {
			health = 15;
			walkAni = "Boss level 2 copy ";
			deathAni = "lvl2_boss_Deathanimation ";
			this.scoreBonus = 250;
			hitPoints = 23;
			this.speed = 0.3;
			this.throwTrash = true;
			chaseHero = true;
			this.setProperties();
		}
		
		override protected function endHurtState():void {
			_hurt = false;
			if (health <= 0) {
				Constants.citrus.state = new LevelThree();
			}
		}
		
	}

}