package game.characters 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.math.MathVector;
	import citrus.objects.platformer.box2d.Enemy;
	import citrus.objects.platformer.box2d.Hero;
	import citrus.objects.platformer.box2d.Platform;
	import citrus.physics.box2d.Box2DUtils;
	import citrus.physics.box2d.IBox2DPhysicsObject;
	import flash.geom.Point;
	import game.elements.GermMonsterHealthBar;
	import game.elements.GermMonsterJunk;
	import game.elements.PoisonousGas;
	import game.elements.SoapBullet;
	import game.Game;
	import starling.display.MovieClip;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Emma
	 */
	public class GermMonster extends Enemy
	{
		// amount of health the player loses
		protected var hitPoints:int = 10;
		
		protected var scoreBonus:int = 0;
		protected var health:int = 1;
		protected var walkAni:String = null;
		protected var deathAni:String = null;
		protected var hurtAni:String = null;
		protected var slimeAni:String = null;
		protected var deathAniLength:int = 1400;
		
		private var state:Game;
		
		private var slime:GermMonsterSlime = null;
		private var healthBar:GermMonsterHealthBar = null;
		
		protected var chaseHero:Boolean = false;
		protected var leaveSlime:Boolean = false;
		protected var throwTrash:Boolean = false;
		protected var emitGas:Boolean = false;
		
		public function GermMonster(name:String, params:Object = null) 
		{
			super(name, params);
			this.hurtDuration = 50;
			trace(this.ID + ", " + this.name);
			state = Constants.citrus.state as Game;
		}
		
		/**
		 * Call after setting the property booleans
		 */
		public function setProperties():void {
			this.view = new MovieClip(Constants.assets.getTextures(walkAni));
			healthBar = new GermMonsterHealthBar(this);
			if (chaseHero) {
				updateCallEnabled = true;
			}
			if (leaveSlime) {
				leaveSlimeTrail();
			}
			if (throwTrash) {
				setTimeout(throwJunk, 3000);
			}
			if (emitGas) {
				setTimeout(emitPoisonousGas, 2000);
			}
		}
		
		public function get bonusScore():int {
			return scoreBonus;
		}
		
		public function get currentHP():int {
			return health;
		}
		
		override protected function endHurtState():void {
			_hurt = false;
			if (health <= 0) {
				if (slime != null) {
					slime.kill = true;
				}
				this.kill = true;
			}
			view = new MovieClip(Constants.assets.getTextures(walkAni), 12);
		}
		
		override public function hurt():void {
			health--;
			healthBar.updateHealth();
			if (health <= 0) {
				this.hurtDuration = deathAniLength;
				Constants.scoreKeeper.enemyDefeated(this);
				view = new MovieClip(Constants.assets.getTextures(deathAni));
			} else if (hurtAni != null) {
				view = new MovieClip(Constants.assets.getTextures(hurtAni));
			}
			super.hurt();
		}
		
		override public function handleBeginContact(contact:b2Contact):void {
			var collider:IBox2DPhysicsObject = Box2DUtils.CollisionGetOther(this, contact);
			
			if (collider is SoapBullet && this.health > 0 )
				hurt();
				
			if (_body.GetLinearVelocity().x < 0 && (contact.GetFixtureA() == _rightSensorFixture || contact.GetFixtureB() == _rightSensorFixture))
				return;
			
			if (_body.GetLinearVelocity().x > 0 && (contact.GetFixtureA() == _leftSensorFixture || contact.GetFixtureB() == _leftSensorFixture))
				return;
			
			if (contact.GetManifold().m_localPoint) {
				
				var normalPoint:Point = new Point(contact.GetManifold().m_localPoint.x, contact.GetManifold().m_localPoint.y);
				var collisionAngle:Number = new MathVector(normalPoint.x, normalPoint.y).angle * 180 / Math.PI;
				
				if ((collider is Platform && collisionAngle != 90) || collider is Enemy)
					turnAround();
			}
		}
		
		override public function update(timeDelta:Number):void {
			super.update(timeDelta);
			if (chaseHero) {
				var char:Hero = Constants.citrus.state.getObjectByName("hero") as Hero;
				if (char != null) {
					if (char.x > this.x) {
						_inverted = false;
					} else if (char.x < this.x) {
						_inverted = true;
					}
				}
			}
		}
		
		public function get hurtPoints():int {
			return hitPoints;
		}
		
		public function leaveSlimeTrail():void {
			slime = new GermMonsterSlime(this, slimeAni);
			Constants.citrus.state.add(slime);
		}
		
		public function throwJunk():void {
			if (health > 0 && Constants.citrus.state === state) {
				setTimeout(throwJunk, 5000);
				Constants.citrus.state.add(new GermMonsterJunk(this, inverted));
				
			}
		}
		
		public function emitPoisonousGas():void {
			if (health > 0 && Constants.citrus.state === state) {
				setTimeout(emitPoisonousGas, 6000);
				Constants.citrus.state.add(new PoisonousGas(this));
			}
		}
		
	}

}