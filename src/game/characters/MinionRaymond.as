package game.characters 
{
	/**
	 * Minion for level 1
	 * @author Emma
	 */
	public class MinionRaymond extends GermMonster
	{
		
		public function MinionRaymond(xpos:int, ypos:int, bounds:int = 100) 
		{
			super("Raymond", { x:xpos, y:ypos, width:50, height:40 } );
			this.rightBound = xpos + bounds;
			this.leftBound = xpos - bounds;
			setUp();
		}
		
		private function setUp():void {
			this.hitPoints = 9;
			this.health = 1;
			this.scoreBonus = 10;
			this.throwTrash = false;
			this.walkAni = "lvl1_germ_WalkANI";
			this.deathAni = "lvl1_germ_DeathANI";
			this.slimeAni = "Slimespore 100px instance";
			this.leaveSlime = true;
			setProperties();
		}
		
	}

}