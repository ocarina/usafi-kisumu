package game.characters 
{
	/**
	 * ...
	 * @author Emma
	 */
	public class MinionAye extends GermMonster
	{
		
		public function MinionAye(xpos:int, ypos:int, bounds:int = 100) 
		{
			super("Aye", { x:xpos, y:ypos, width:50, height:60 } );
			this.rightBound = xpos + bounds;
			this.leftBound = xpos - bounds;
			setUp();
		}
		
		private function setUp():void {
			this.speed = 0.5;
			this.hitPoints = 14;
			this.health = 2;
			this.throwTrash = true;
			this.deathAniLength = 1150;
			this.scoreBonus = 15;
			this.hurtDuration = 500;
			this.walkAni = "Minion instance";
			this.deathAni = "lvl 2_minion_death instance";
			this.hurtAni = "lvl2minion_gothit";
			setProperties();
		}
		
	}

}