package game.characters 
{
	import game.GameOver;
	/**
	 * ...
	 * @author ...
	 */
	public class MonsterJouke extends GermMonster
	{
		
		public function MonsterJouke(xpos:int, ypos:int) 
		{
			super("JoukeBoss", { x:xpos, y:ypos, width:140, height:120 } );
			setUp();
		}
		
		private function setUp():void {
			health = 20;
			walkAni = "lvl3_boss_walk";
			deathAni = "lvl3_boss_Deathani";
			this.scoreBonus = 350;
			hitPoints = 26;
			this.speed = 0.4;
			this.throwTrash = true;
			chaseHero = true;
			this.setProperties();
		}
		
		override protected function endHurtState():void {
			_hurt = false;
			if (health <= 0) {
				Constants.citrus.state = new GameOver();
			}
		}
		
	}

}