package game.characters 
{
	import Box2D.Dynamics.Contacts.b2Contact;
	import citrus.objects.Box2DPhysicsObject;
	import citrus.objects.CitrusSprite;
	import citrus.physics.PhysicsCollisionCategories;
	import starling.display.MovieClip;
	/**
	 * ...
	 * @author Emma
	 */
	public class GermMonsterSlime extends Box2DPhysicsObject
	{
		private var germMonster:GermMonster = null;
		
		public function GermMonsterSlime(monster:GermMonster, animation:String) 
		{
			super("slime", { x:monster.x, y:monster.y - monster.height / 2, width:monster.width * 2, height:1, view:new MovieClip(Constants.assets.getTextures(animation)) } );
			germMonster = monster;
			this.updateCallEnabled = true;
		}
		
		override protected function defineFixture():void {
			super.defineFixture();
			_fixtureDef.filter.categoryBits = PhysicsCollisionCategories.Get("Items");
		}
		
		override public function update(timeDelta:Number):void {
			super.update(timeDelta);
			if (!germMonster.inverted) {
				if (this.x < germMonster.x - germMonster.width / 2) {
					this.x = germMonster.x - germMonster.width / 2;
				}
			} else {
				if (this.x > germMonster.x + germMonster.width / 2) {
					this.x = germMonster.x + germMonster.width / 2;
				}
			}
		}
		
	}

}