package game.characters 
{
	import game.LevelTwo;
	import menu.StartMenu;
	import starling.display.Image;
	/**
	 * Germ boss level 1
	 * @author Emma
	 */
	public class MonsterRaymond extends GermMonster
	{
		
		public function MonsterRaymond(xpos:int, ypos:int) 
		{
			super("RaymondBoss", { x:xpos, y:ypos, width:110, height:100 } );
			setUp();
		}
		
		private function setUp():void {
			health = 10;
			walkAni = "lvl1_boss_walk_ani";
			deathAni = "lvl1_boss_Deathani";
			this.scoreBonus = 200;
			hitPoints = 19;
			this.speed = 0.5;
			chaseHero = true;
			this.setProperties();
		}
		
		override protected function endHurtState():void {
			_hurt = false;
			if (health <= 0) {
				Constants.citrus.state = new LevelTwo();
			}
		}
		
	}

}