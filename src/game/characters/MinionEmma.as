package game.characters 
{
	/**
	 * ...
	 * @author Emma
	 */
	public class MinionEmma extends GermMonster
	{
		
		public function MinionJouke(xpos:int, ypos:int, bounds:int = 100) 
		{
			super("Emma", { x:xpos, y:ypos, width:50, height:50 } );
			this.rightBound = xpos + bounds;
			this.leftBound = xpos - bounds;
			setUp();
		}
		
		private function setUp():void {
			this.hitPoints = 30;
			this.health = 3;
			this.scoreBonus = 25;
			this.emitGas = true;
			this.leaveSlime = true;
			this.walkAni = "lvl3minion_walk ";
			this.deathAni = "lvl3minion_death ";
			this.hurtAni = "lvl3minion_gothit";
			this.slimeAni = "Slimespore 100px instance";
			setProperties();
		}
		
	}

}