package game 
{
	import citrus.core.starling.StarlingState;
	import data.Settings;
	import feathers.controls.TextInput;
	import game.elements.PlayerData;
	import game.pointandclick.PointAndClick;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	/**
	 * ...
	 * @author Emma
	 */
	public class ChooseCharacter extends StarlingState
	{
		private var textField:TextInput;
		private var startButton:Button;
		private var boyButton:Sprite;
		private var girlButton:Sprite;
		private var selectionQuad:Quad;
		private var selection:String = "boy";
		
		private var xBoySelection:int = 100;
		private var yBoySelection:int = 100;
		private var xGirlSelection:int = 250;
		private var yGirlSelection:int = 100;
		
		public function ChooseCharacter() 
		{
			addChild(new Image(Constants.assets.getTexture("Character_Selection_Screen instance 10000")));
			createTextField();
			createButtons();
		}
		
		private function createButtons():void {
			startButton = new Button(Constants.assets.getTexture("mainmenu_playbutton.png"));
			startButton.x = Constants.STAGE_WIDTH - 5 - startButton.width;
			startButton.y = Constants.STAGE_HEIGHT - 5 - startButton.height;
			startButton.addEventListener(Event.TRIGGERED, startGame);
			addChild(startButton);
			
			selectionQuad = new Quad(120, 120, 0x00FF00);
			selectionQuad.x = xBoySelection;
			selectionQuad.y = yBoySelection;
			addChild(selectionQuad);
			
			boyButton = new Sprite();
			boyButton.x = xBoySelection + 5;
			boyButton.y = yBoySelection + 5;
			boyButton.addEventListener(TouchEvent.TOUCH, clickBoy);
			boyButton.addChild(new Image(Constants.assets.getTexture("Male_Head_Selectionscreen instance 10000")));
			addChild(boyButton);
			
			girlButton = new Sprite();
			girlButton.x = xGirlSelection + 5;
			girlButton.y = yGirlSelection + 5;
			girlButton.addEventListener(TouchEvent.TOUCH, clickGirl);
			girlButton.addChild(new Image(Constants.assets.getTexture("Female_Head_Selectionscreen instance 10000")));
			addChild(girlButton);
		}
		
		private function createTextField():void {
			textField = new TextInput();
			textField.width = 100;
			textField.height = 20;
			textField.text = Constants.settings.getCurrentName();
			textField.visible = true;
			textField.x = Constants.STAGE_WIDTH / 2 - textField.width / 2;
			textField.y = 250;
			addChild(textField);
		}
		
		private function clickBoy():void {
			selectionQuad.x = xBoySelection;
			selectionQuad.y = yBoySelection;
			selection = "boy";
		}
		
		private function clickGirl():void {
			selectionQuad.x = xGirlSelection;
			selectionQuad.y = yGirlSelection;
			selection = "girl";
		}
		
		private function startGame():void {
			Constants.settings.setCurrentName(textField.text);
			new PlayerData(textField.text, selection);
			Constants.citrus.state = new LevelOne();
		}
		
	}

}