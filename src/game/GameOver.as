package game 
{
	import citrus.core.starling.StarlingState;
	import data.HighScoreManager;
	import flash.utils.setTimeout;
	import game.elements.PlayerData;
	import game.elements.ScoreKeeper;
	import menu.StartMenu;
	import starling.text.TextField;
	/**
	 * ...
	 * @author Emma
	 */
	public class GameOver extends StarlingState
	{
		
		public function GameOver() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			var score:int = Constants.scoreKeeper.currentScore;
			var highScore:Boolean = HighScoreManager.getManager().placeScore(PlayerData.playerData.name, score);
			var scoreView:TextField = new TextField(300, 50, "Your score: " + score, "Verdana", 18);
			scoreView.x = Constants.STAGE_WIDTH / 2 - scoreView.width / 2;
			scoreView.y = 90;
			addChild(scoreView);
			Constants.scoreKeeper.setCurrentScore(0);
			
			var text:TextField = new TextField(200, 50, "Thanks for testing!", "Verdana", 20);
			text.x = Constants.STAGE_WIDTH / 2 - text.width / 2;
			text.y = 150;
			addChild(text);
			
			setTimeout(toStart, 2000);
		}
		
		private function toStart():void {
			Constants.citrus.state = new StartMenu();
		}
		
	}

}