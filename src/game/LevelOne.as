package game 
{
	import citrus.core.CitrusObject;
	import citrus.objects.CitrusSprite;
	import citrus.objects.platformer.box2d.Coin;
	import game.characters.MinionAye;
	import game.characters.MinionRaymond;
	import game.characters.Protagonist;
	import game.collectables.AlcoholGel;
	import game.collectables.QuestionMark;
	import game.collectables.Soap;
	import game.collectables.Water;
	import game.elements.CutScene;
	import game.elements.DirtyWater;
	import game.elements.EndLevelTrigger;
	import game.elements.Ladder;
	import game.elements.PlayerData;
	import game.elements.Trampoline;
	import game.elements.TutorialTrigger;
	import game.pointandclick.PointAndClick;
	import starling.display.Image;
	import starling.events.Event;
	import flash.utils.setTimeout;
	import starling.events.TouchEvent;
	/**
	 * ...
	 * @author Emma
	 */
	public class LevelOne extends Game
	{
		
		public function LevelOne() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			createLevel(Constants.assets.getXml("LVL1"), Constants.assets.getTextureAtlas("tiles2"));
			add(new EndLevelTrigger(BossOne, 1, 3, { x:5660 + 155, y:835, width:5, height:5 } ));
			add(new TutorialTrigger(574 + 155, 830, 8));
			createProtagonist(350, 750, true);
			createEnemies();
			createCollectables();
			handleTutorial(0, true);
		}
		
		private function createEnemies():void {
			add(new MinionRaymond(1130+124, 664, 50));
			add(new MinionRaymond(510+124, 820, 30));
			add(new MinionRaymond(1504+124, 820));
			add(new MinionRaymond(2280+124, 820));
			add(new MinionRaymond(3937+124, 820));
			add(new MinionRaymond(5068 + 124, 820));
			add(new MinionRaymond(3442 + 155, 820, 30));
			add(new MinionRaymond(5612 + 155, 590, 50));
			
			add(new DirtyWater(2934 - 5, 888));
			add(new DirtyWater(2961 - 5, 888));
			add(new DirtyWater(2992 - 5, 888));
			add(new DirtyWater(3023 - 5, 888));
			add(new DirtyWater(3054 - 5, 888));
			add(new DirtyWater(3085 - 5, 888));
			add(new DirtyWater(3116 - 5, 888));
			add(new DirtyWater(3147 - 5, 888));
			add(new DirtyWater(3178 - 5, 888));
			add(new DirtyWater(3209 - 5, 888));
			add(new DirtyWater(3240 - 5, 888));
			add(new DirtyWater(3271 - 5, 888));
			add(new DirtyWater(3297, 888));
			add(new DirtyWater(3328, 888));
			add(new DirtyWater(3359, 888));
			add(new DirtyWater(3390, 888));
			add(new DirtyWater(3421, 888));
			add(new DirtyWater(3452, 888));
			add(new DirtyWater(3482, 888));
			add(new DirtyWater(3510, 888));
			add(new DirtyWater(3525, 888));
			
			add(new DirtyWater(4790, 888));
			add(new DirtyWater(4821, 888));
			add(new DirtyWater(4852, 888));
		}
		
		private function createCollectables():void {
			createSoapBars();
			createWaterBuckets();
			createAlcoholGels();
			
			add(new QuestionMark(3425 + 155, 520));
		}
		
		override public function createSoapBars():void {
			add(new Soap(268+124, 746));
			add(new Soap(1904+124, 588));
		}
		
		override public function createWaterBuckets():void {
			add(new Water(320+124, 746));
			add(new Water(1570+124, 610));
		}
		
		override public function createAlcoholGels():void {
			add(new AlcoholGel(5440 + 155, 660));
			add(new AlcoholGel(2899 + 155, 444));
			add(new AlcoholGel(5518 + 155, 838));
		}
		
	}
	
}