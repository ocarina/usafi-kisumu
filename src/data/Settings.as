package data 
{
	import flash.net.SharedObject;
	/**
	 * ...
	 * @author ...
	 */
	public class Settings 
	{
		private var settings:SharedObject;
		private static var instance:HighScoreManager;
		
		public function Settings() 
		{
			settings = SharedObject.getLocal("settings");
		}
		
		public function setVolume(volume:int):void {
			if (volume > 100) {
				volume = 100;
			} else if (volume < 0) {
				volume = 0;
			}
			settings.data.volume = volume;
		}
		
		public function getVolume():int {
			if (settings.data.volume == undefined) {
				settings.data.volume = 100;
			}
			return settings.data.volume as int;
		}
		
		public function setCurrentName(name:String):void {
			settings.data.currentName = name;
		}
		
		public function getCurrentName():String {
			if (settings.data.currentName == undefined) {
				settings.data.currentName = "Kirimi";
			}
			return settings.data.currentName;
		}
		
		public function setTutorialTriggered(isTriggered:Boolean):void {
			settings.data.tutorialTriggered = isTriggered;
		}
		
		public function getTutorialTriggered():Boolean {
			if (settings.data.tutorialTriggered == undefined) {
				settings.data.tutorialTriggered = false;
			}
			return settings.data.tutorialTriggered;
		}
		
		public function setLadderTutorialTriggered(isTriggered:Boolean):void {
			settings.data.ladderTutorialTriggered = isTriggered;
		}
		
		public function getLadderTutorialTriggered():Boolean {
			if (settings.data.ladderTutorialTriggered == undefined) {
				settings.data.ladderTutorialTriggered = false;
			}
			return settings.data.ladderTutorialTriggered as Boolean;
		}
		
	}

}