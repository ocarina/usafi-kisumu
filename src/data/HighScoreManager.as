package data 
{
	import flash.net.SharedObject;
	/**
	 * ...
	 * @author ...
	 */
	public class HighScoreManager 
	{
		private var highscores:SharedObject;
		private static var instance:HighScoreManager;
		
		public function HighScoreManager() 
		{
			highscores = SharedObject.getLocal("highscores");
			if (highscores.data.scores == undefined) {
				resetList();
			}
		}
		
		public static function getManager():HighScoreManager {
			if (instance == null) {
				instance = new HighScoreManager();
			}
			return instance;
		}
		
		public function resetList():void {
			highscores.data.scores = new Array(
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 },
									{ name:"...", score:0 } );
			highscores.flush();
		}
		
		public function getScores():Array {
			var array:Array = highscores.data.scores as Array;
			trace(array[0].name);
			return array;
		}
		
		public function getScoresAsStrings():Array {
			var array:Array = getScores();
			var newArray:Array = new Array();
			for (var i:int = 0; i < array.length; i++) {
				var string:String = i + 1 + ". " + array[i].name + ": " + array[i].score;
				newArray.push(string);
			}
			return newArray;
		}
		
		/**
		 * Place a highscore in the list
		 * @return true if score is high enough to be on the list, false if otherwise
		 */
		public function placeScore(playerName:String, playerScore:int):Boolean {
			var i:int = 0;
			var array:Array = getScores();
			var highestScore:int = array[i].score as int;
			while (playerScore < highestScore) {
				i++;
				if (i > 9) return false;
				highestScore = array[i].score as int;
			}
			array.splice(i, 0, { name:playerName, score:playerScore } );
			array.pop();
			highscores.data.scores = array;
			highscores.flush();
			return true;
		}
		
	}

}