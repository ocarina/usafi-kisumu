package  
{
	import citrus.core.starling.StarlingCitrusEngine;
	import citrus.core.starling.StarlingState;
	import citrus.core.starling.ViewportMode;
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import game.Game;
	import game.GameOver;
	import menu.StartMenu;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.core.Starling;
	/**
	 * ...
	 * @author Emma
	 */
	public class Main extends StarlingCitrusEngine
	{
		private static var assetManager:AssetManager;
		
		public function Main() 
		{
			super();
			createViewPort();
			Starling.multitouchEnabled = true;
			Constants.citrus = this;
		}
		
		override public function initialize():void {
			super.initialize();
			setUpStarling(true);
		}
		
		override public function handleStarlingReady():void {
			stage.color = 0x000000;
			state = new Loader();
		}
		
		private function createViewPort():void {
			_baseWidth = 480;
			_baseHeight = 320;
			_viewportMode = ViewportMode.LETTERBOX;
		}
		
		public function changeState(newState:String):void {
			var newScreen:StarlingState = null;
			switch (newState) {
				case "menu": 
					newScreen = new StartMenu();
					break;
				case "game":
					newScreen = new Game();
					break;
				case "dieded":
					newScreen = new GameOver();
					break;
				default:
					newScreen = new StartMenu();
					break;
			}
			state = newScreen;
		}
		
		public static function get assets():AssetManager {
			return assetManager;
		}
		
		
		
	}

}