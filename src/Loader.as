package  
{
	import citrus.core.starling.StarlingState;
	import flash.display.Bitmap;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	import menu.StartMenu;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	/**
	 * ...
	 * @author ...
	 */
	public class Loader extends StarlingState
	{
		private var loadingScreen:Image;
		private var loadingBar:Quad;
		private var assetManager:AssetManager;
		
		[Embed(source="../bin/textures/1x/loadingScreen.png")]
		public static const LoadScreen:Class;
		
		public function Loader() 
		{
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			var bitmap:Bitmap = new LoadScreen();
			loadingScreen = new Image(Texture.fromBitmap(bitmap));
			addChild(loadingScreen);
			createAssetManager();
		}
		
		public function createAssetManager():void {
			var scaleFactor:int = 1;
			var appDir:File = File.applicationDirectory;
            assetManager = new AssetManager(scaleFactor);
			Constants.assets = assetManager;
            assetManager.verbose = Capabilities.isDebugger;
            assetManager.enqueue(
                appDir.resolvePath("audio"),
                appDir.resolvePath("fonts/1x"),
                appDir.resolvePath("textures/1x"),
				appDir.resolvePath("xml")
            );
			
			loadingBar = new Quad(1, 24, 0x43B76A);
			loadingBar.x = 132;
			loadingBar.y = 148;
			addChild(loadingBar);
			assetManager.loadQueue(function onProgress(ratio:Number):void {
				loadingBar.width = Math.ceil(ratio * 224);
				if (ratio == 1) {
					removeChild(loadingBar);
					removeChild(loadingScreen);
					var start:StartMenu = new StartMenu();
					Constants.citrus.state = start;
				}
			});
		}
		
	}

}